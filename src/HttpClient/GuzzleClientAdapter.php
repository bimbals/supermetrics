<?php

namespace App\HttpClient;

use GuzzleHttp\ClientInterface as GuzzleClientInterface;

class GuzzleClientAdapter implements HttpClientInterface
{
    private $guzzle;
    private $statusCode;

    public function __construct(GuzzleClientInterface $guzzle)
    {
        $this->guzzle = $guzzle;
    }

    public function request($method, $uri, array $options = []): string
    {
        // suppress guzzle exceptions and pass options in form_params key
        $options = array_merge(['http_errors' => false], ['form_params' => $options]);

        $response = $this->guzzle->request($method, $uri, $options);

        $this->statusCode = $response->getStatusCode();

        return $response->getBody()->getContents();
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }
}