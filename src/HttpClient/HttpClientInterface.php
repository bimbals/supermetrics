<?php

declare(strict_types=1);

namespace App\HttpClient;

interface HttpClientInterface
{
    public function request(string $method, string $uri, array $options = []): string;

    public function getStatusCode(): int;
}