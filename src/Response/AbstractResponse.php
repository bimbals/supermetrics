<?php

declare(strict_types=1);

namespace App\Response;

abstract class AbstractResponse
{
    public function showResponse(): string
    {
        $headers = $this->getHeaders();

        foreach ($headers as $type => $value) {
            header(sprintf('%s: %s', $type, $value));
        }

        return $this->getContent();
    }

    public abstract function getHeaders(): array;

    public abstract function getContent(): ?string;
}