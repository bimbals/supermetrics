<?php

declare(strict_types=1);

namespace App\Response;

class JsonResponse extends AbstractResponse
{
    private $data = [];
    private $statusCode;

    public function __construct(array $data, int $statusCode = 200)
    {
        $this->data = $data;
        $this->statusCode = $statusCode;
    }

    public function getContent(): ?string
    {
        return json_encode($this->data);
    }

    public function getHeaders(): array
    {
        return array_merge(
            ['Content-Type' => 'application/json'],
            $this->getStatusCodeHeaders(),
        );
    }

    // TODO: implement some third-party library
    private function getStatusCodeHeaders(): array
    {
        switch ($this->statusCode) {
            case 500:
                $headers = ['HTTP/1.1 500 Internal Server Error'];
                break;
            default:
                $headers = ['HTTP/1.1 200 OK'];
        }

        return $headers;
    }
}