<?php

declare(strict_types=1);

namespace App\Persistence;

class PostPersistence extends AbstractBasePersistence implements PersistenceInterface
{
    private const API_URI_POSTS = 'https://api.supermetrics.com/assignment/posts';
    protected $options = [
        'page' => 1,
    ];

    public function retrieve(array $options = array()): array
    {
        $this->setOptions($options);

        $options = ['page' => $this->options['page']];

        $uri = self::API_URI_POSTS . '?' . http_build_query($options);

        $data = $this->callApi('GET', $uri);

        return $data['posts'] ?? [];
    }

    protected function setOptions(array $options)
    {
        $options['page'] = isset($options['page']) ? (int)$options['page'] : null;

        $this->options['page'] = $options['page'] ?? 1;
        // TODO: implement more options
    }
}