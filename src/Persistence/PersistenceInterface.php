<?php

declare(strict_types=1);

namespace App\Persistence;

interface PersistenceInterface
{
    /**
     * @param array $options E.g. id, page
     */
    public function retrieve(array $options = []): array;

    // TODO: implement insert and delete
}