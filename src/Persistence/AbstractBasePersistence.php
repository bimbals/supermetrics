<?php

declare(strict_types=1);

namespace App\Persistence;

use App\HttpClient\HttpClientInterface;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

abstract class AbstractBasePersistence
{
    private const API_URI_TOKEN = 'https://api.supermetrics.com/assignment/register';
    private const API_CREDENTIALS_FILEPATH = __DIR__ . '/../../config/api_credentials.yaml';
    private const API_INVALID_TOKEN_MESSAGES = ['Invalid SL Token', 'Required parameter \'sl_token\' unavailable.'];

    protected $token;
    private $httpClient;

    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @return mixed|null
     */
    public function callApi(string $method, string $uri, array $options = [], bool $wasExecuted = false)
    {
        try {

            $response = $this->requestClient($method, $uri, $options);

            return $this->parseAPIResponse($response);

        } catch (\InvalidArgumentException $e) {

            if ($wasExecuted) {
                throw new \UnexpectedValueException('Invalid token');
            }

            // repeat request
            return $this->resetToken(
                function () use ($method, $uri, $options) {
                    return $this->callApi($method, $uri, $options, true);
                }
            );
        }
    }

    /**
     * @return mixed
     */
    private function requestClient(string $method, string $uri, array $options = [])
    {
        switch ($method) {
            case 'GET':
                $parsedUri = parse_url($uri);
                $separator = (null === $parsedUri['query']) ? '?' : '&';
                $uri .= $separator . 'sl_token=' . $this->token;
                break;

            case 'POST':
                $options['sl_token'] = $this->token;
                break;
        }

        return $this->httpClient->request($method, $uri, $options);
    }

    /**
     * @return mixed|null
     *
     * TODO: move into separate file e.g. APIResponseHandler
     */
    private function parseAPIResponse(string $response)
    {
        try {
            $data = json_decode($response, true, 512, JSON_THROW_ON_ERROR);

            $error = $data['error']['message'] ?? null;

            if ($error) {

                // throw different exception for token revalidation
                $isInvalidToken = in_array($error, self::API_INVALID_TOKEN_MESSAGES);
                if ($isInvalidToken) {
                    throw new \InvalidArgumentException(sprintf('Revalidate token: %s', $error));
                }

                throw new \UnexpectedValueException(sprintf('API Response has some errors: %s', $error));
            }

            return $data['data'] ?? null;
        } catch (\JsonException $e) {
            throw new \UnexpectedValueException(sprintf('API Response can\'t be decoded: %s', $e->getMessage()));
        }
    }

    private function resetToken(callable $resumeCallable)
    {
        $credentials = $this->getCredentials();

        $response = $this->httpClient->request('POST', self::API_URI_TOKEN, $credentials);

        $data = $this->parseAPIResponse($response);

        // set new token
        $this->token = $data['sl_token'] ?? null;

        if (!$this->token) {
            throw new \InvalidArgumentException(sprintf('Didn\'t receive token or invalid credentials'));
        }

        return $resumeCallable();
    }

    // TODO: move into separate file, ex: CredentialsReader
    private function getCredentials(): array
    {
        try {
            $credentials = Yaml::parseFile(self::API_CREDENTIALS_FILEPATH);

            return [
                'client_id' => $credentials['client_id'],
                'email' => $credentials['email'],
                'name' => $credentials['name'],
            ];

        } catch (ParseException $e) {
            throw new \UnexpectedValueException(sprintf('Unable to parse the YAML file: %s', $e->getMessage()));
        }
    }
}