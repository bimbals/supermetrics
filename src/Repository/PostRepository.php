<?php

declare(strict_types=1);

namespace App\Repository;

use App\Model\Post;

class PostRepository extends AbstractRepository
{
    public function getObjectFromState(array $data): Post
    {
        // TODO: validate state
        return Post::fromState($data);
    }

    // TODO: implement some logic not to query for posts if return value of "page" is different than requested
    public function findAllThroughPages(int $pageCount = 1): array
    {
        $result = [];

        for ($page = 1; $page <= $pageCount; $page++) {
            $result = array_merge(
                $result,
                parent::findAll(['page' => $page])
            );
        }

        return $result;
    }
}