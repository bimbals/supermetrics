<?php

declare(strict_types=1);

namespace App\Repository;

interface RepositoryInterface
{
    // TODO: implement later
    //public function find(int $id);

    public function findAll(array $options = array()): array;
}
