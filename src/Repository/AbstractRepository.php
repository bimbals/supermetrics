<?php

declare(strict_types=1);

namespace App\Repository;

use App\Persistence\PersistenceInterface;

abstract class AbstractRepository implements RepositoryInterface
{
    private $persistence;

    public function __construct(PersistenceInterface $persistence)
    {
        $this->persistence = $persistence;
    }

    public function findAll(array $options = array()): array
    {
        $arrayData = $this->persistence->retrieve($options);

        return $this->getObjectsFromState($arrayData);
    }

    public function getObjectsFromState(array $arrayData): array
    {
        $result = [];

        foreach ($arrayData as $data) {
            array_push($result, $this->getObjectFromState($data));
        }

        return $result;
    }

    /**
     * @return mixed
     */
    public abstract function getObjectFromState(array $data);
}