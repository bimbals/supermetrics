<?php

declare(strict_types=1);

namespace App\Model;

class Post
{
    private $id;
    private $message;
    private $type;
    private $createdTime;
    private $user;

    public function __construct(?string $id, string $message, string $type, string $createdTime, User $user)
    {
        $this->id = $id;
        $this->message = $message;
        $this->type = $type;
        $this->createdTime = new \DateTime($createdTime);
        $this->user = $user;

        if ($this->id) {
            $this->user->addPost($this);
        }
    }

    public static function fromState(array $state): Post
    {
        return new static(
            $state['id'],
            $state['message'],
            $state['type'],
            $state['created_time'],
            new User($state['from_id'], $state['from_name'])
        );
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getCreatedTime(): \DateTime
    {
        return $this->createdTime;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function __toString()
    {
        return $this->getId();
    }

    public function getId(): ?string
    {
        return $this->id;
    }
}