<?php

declare(strict_types=1);

namespace App\Model;

class User
{
    private $username;
    private $email;
    private $posts;

    public function __construct(?string $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
        $this->posts = new \ArrayObject();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function addPost(Post $post)
    {
        $this->posts->append($post);
    }

    public function getPosts(): iterable
    {
        return $this->posts;
    }
}