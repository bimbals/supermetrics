<?php

declare(strict_types=1);

namespace App\Service;

use App\Model\Post;

class PostStatisticsHelper implements PostStatisticsHelperInterface
{
    /**
     * @param Post[] $posts
     */
    public static function getPostAverageCharacterLengthPerMonth(array $posts): array
    {
        $result = [];
        $tmpPostMap = [];

        foreach ($posts as $post) {
            $tmpPostMap[$post->getCreatedTime()->format('Y-m')][] = mb_strlen($post->getMessage());
        }

        foreach ($tmpPostMap as $date => $messageLengthArray) {

            $monthItemsCount = count($tmpPostMap[$date]);
            $monthItemsSum = 0;

            foreach ($messageLengthArray as $messageLength) {
                $monthItemsSum += $messageLength;
            }

            $result[$date] = round($monthItemsSum / $monthItemsCount);
        }

        return $result;
    }

    /**
     * @param Post[] $posts
     */
    public static function getLongestPostByCharacterLengthPerMonth(array $posts): array
    {
        $result = [];

        foreach ($posts as $post) {
            $postDate = $post->getCreatedTime()->format('Y-m');

            $result[$postDate] = $result[$postDate] ?? [
                    'id' => $post->getId(),
                    'messageLength' => mb_strlen($post->getMessage()),
                ];

            $currPostMessageLength = mb_strlen($post->getMessage());
            if ($currPostMessageLength > $result[$postDate]['messageLength']) {
                $result[$postDate] = [
                    'id' => $post->getId(),
                    'messageLength' => $currPostMessageLength,
                ];
            }
        }

        return $result;
    }

    /**
     * @param Post[] $posts
     */
    public static function getTotalPostsSplitByWeek(array $posts): array
    {
        $result = [];

        foreach ($posts as $post) {
            $postDate = $post->getCreatedTime()->format('W');

            $result[$postDate] = $result[$postDate] ?? 0;
            $result[$postDate]++;
        }

        return $result;
    }

    /**
     * @param Post[] $posts
     */
    public static function getAverageNumberOfPostsPerUserPerMonth(array $posts): array
    {
        $result = [];
        $tmpPostMap = [];

        foreach ($posts as $post) {
            $postDate = $post->getCreatedTime()->format('Y-m');
            $userId = $post->getUser()->getId();

            $tmpPostMap[$postDate][$userId] = $tmpPostMap[$postDate][$userId] ?? 0;
            $tmpPostMap[$postDate][$userId]++;
        }

        foreach ($tmpPostMap as $date => $userArray) {

            $monthIUsersCount = count($tmpPostMap[$date]);
            $monthPostCountSum = 0;

            foreach ($userArray as $postCount) {
                $monthPostCountSum += $postCount;
            }

            $result[$date] = round($monthPostCountSum / $monthIUsersCount);
        }

        return $result;
    }

}