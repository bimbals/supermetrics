<?php

declare(strict_types=1);

namespace App\Service;

interface PostStatisticsHelperInterface
{
    public static function getPostAverageCharacterLengthPerMonth(array $posts): array;
    public static function getLongestPostByCharacterLengthPerMonth(array $posts): array;
    public static function getTotalPostsSplitByWeek(array $posts): array;
    public static function getAverageNumberOfPostsPerUserPerMonth(array $posts): array;
}