<?php

declare(strict_types=1);

namespace App\Controller;

use App\HttpClient\GuzzleClientAdapter;
use App\Model\Post;
use App\Persistence\PostPersistence;
use App\Repository\PostRepository;
use App\Response\AbstractResponse;
use App\Response\JsonResponse;
use App\Service\PostStatisticsHelper;
use GuzzleHttp\Client as GuzzleClient;
use Iterator;

class MainController
{
    public function handle(): AbstractResponse
    {
        try {
            $httpClient = new GuzzleClientAdapter(new GuzzleClient());

            $postRepository = new PostRepository(new PostPersistence($httpClient));

            /** @var Post[]|Iterator $posts */
            $posts = $postRepository->findAllThroughPages(10);

            return new JsonResponse([
                'data' => [
                    'Average character length of a post / month' => PostStatisticsHelper::getPostAverageCharacterLengthPerMonth($posts),
                    'Longest post by character length / month' => PostStatisticsHelper::getLongestPostByCharacterLengthPerMonth($posts),
                    'Total posts split by week ' => PostStatisticsHelper::getTotalPostsSplitByWeek($posts),
                    'Average number of posts per user / month' => PostStatisticsHelper::getAverageNumberOfPostsPerUserPerMonth($posts),
                ]
            ]);

        } catch (\Exception $e) {
            return new JsonResponse([
                'error' => $e->getMessage(),
            ]);
        }
    }
}