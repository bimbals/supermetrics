<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// bootstrap some vendors
require __DIR__.'/vendor/autoload.php';

// TODO: implement some routing (route -> controller) logic
$mainController = new App\Controller\MainController();
$response = $mainController->handle();

echo $response->showResponse();