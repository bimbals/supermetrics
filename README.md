How to start

**1. Create credentials file api_credentials.yaml from template api_credentials.yaml.dist in /config/ folder**
**2. Execute docker via command: ** `docker-compose up -d`
**3. Run in docker container command: **`composer install`
**3. Open localhost server through browser**

